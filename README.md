
### devcalc

A simple command line calculator with support integer of binary, octal, decimal, hexadecimal number code.

* `cargo run "2*(2+2)"`
* `cargo run "0b1111*(0x0F+15)"`
* `cargo run 0xABCD`

### TODO

* rework to be more rust idiomatic
* support of negative numbers pass to command line
* floating point numbers
