use itertools::Itertools;

use std::collections::VecDeque;
use std::env;

#[derive(Debug, PartialEq, Clone, Copy)]
enum Token {
  Operator(char),
  Number(i64),
}

#[derive(Debug, PartialEq)]
enum Error {
  Parse(String),
  Evaluate(String),
}

fn is_number(c: char) -> bool {
  match c {
    '0'..='9' | 'x' | 'X' | 'a'..='f' | 'A'..='F' => true,
    _ => false,
  }
}

fn is_operator(c: char) -> bool {
  match c {
    '+' | '-' | '*' | '/' | '(' | ')' => true,
    _ => false,
  }
}

impl std::convert::From<std::num::ParseIntError> for Error {
  fn from(_: std::num::ParseIntError) -> Error {
    Error::Parse(String::from("Incorrect number"))
  }
}

fn parse_number(number: &str) -> Result<Token, Error> {
  if number.starts_with("0x") {
    Ok(Token::Number(i64::from_str_radix(&number[2..], 16)?))
  } else if number.starts_with("0b") {
    Ok(Token::Number(i64::from_str_radix(&number[2..], 2)?))
  } else if number.starts_with('0') && number.len() > 1 {
    Ok(Token::Number(i64::from_str_radix(&number[1..], 8)?))
  } else {
    Ok(Token::Number(number.parse::<i64>()?))
  }
}

fn tokenize(expr: &str) -> Result<Vec<Token>, Error> {
  let mut tokens = Vec::<Token>::new();
  let mut number = String::new();
  for item in expr.chars() {
    if is_number(item) {
      let lower = item.to_ascii_lowercase();
      number.push(lower);
    } else if is_operator(item) {
      if !number.is_empty() {
        tokens.push(parse_number(&number)?);
        number.clear();
      }
      tokens.push(Token::Operator(item));
    } else {
      return Err(Error::Parse(String::from("Not recognize input")));
    }
  }

  if !number.is_empty() {
    tokens.push(parse_number(&number)?);
  }
  Ok(tokens)
}

fn get_priority(operator: &Token) -> u8 {
  if let Token::Operator(operator) = operator {
    match operator {
      '+' => return 1,
      '-' => return 1,
      '*' => return 2,
      '/' => return 2,
      '(' => return 3,
      ')' => return 3,
      _ => panic!(),
    }
  }
  panic!();
}

fn copy_operators(from: &[Token], to: &mut Vec<Token>) {
  to.extend(
    from
      .iter()
      .rev()
      .filter(|&op| *op != Token::Operator('('))
      .copied(),
  );
}

fn postfix(tokens: Vec<Token>) -> Result<Vec<Token>, Error> {
  let mut result = Vec::<Token>::new();
  let mut operators = Vec::<Token>::new();

  for token in tokens.into_iter() {
    match token {
      Token::Number(v) => result.push(Token::Number(v)),
      Token::Operator(op) => match op {
        '(' => {
          operators.push(token);
        }
        ')' => {
          copy_operators(&operators, &mut result);
          operators.clear();
        }
        _ => {
          if operators.is_empty()
            || operators.iter().last() == Some(&Token::Operator('('))
            || get_priority(&operators[operators.len() - 1]) < get_priority(&token)
          {
            operators.push(token);
          } else {
            while !operators.is_empty()
              && get_priority(&operators[operators.len() - 1]) >= get_priority(&token)
            {
              copy_operators(&operators, &mut result);
              operators.pop();
            }
            operators.push(Token::Operator(op));
          }
        }
      },
    }
  }
  operators.iter().rev().for_each(|op| result.push(*op));
  Ok(result)
}

fn calc(op: char, num1: &i64, num2: &i64) -> Result<i64, Error> {
  match op {
    '+' => Ok(num1 + num2),
    '-' => Ok(num1 - num2),
    '*' => Ok(num1 * num2),
    '/' => {
      if *num2 == 0 {
        Err(Error::Evaluate(String::from("Divide by zero")))
      } else {
        Ok(num1 / num2)
      }
    }
    _ => Err(Error::Evaluate(String::from("Not supported operator"))),
  }
}

fn evaluate(tokens: Vec<Token>) -> Result<i64, Error> {
  let mut mid_result = Vec::<Token>::new();
  for token in tokens.into_iter() {
    match token {
      Token::Number(_) => mid_result.push(token),
      Token::Operator(op) => {
        let numbers: Vec<&Token> = mid_result.iter().rev().take(2).rev().collect();
        match numbers[..] {
          [Token::Number(num1), Token::Number(num2)] => {
            let res = calc(op, num1, num2)?;
            mid_result.pop();
            mid_result.pop();
            mid_result.push(Token::Number(res));
          }
          _ => {
            return Err(Error::Evaluate(String::from("Expected number")));
          }
        }
      }
    }
  }
  mid_result.last().map_or(
    Err(Error::Evaluate(String::from("Missing result"))),
    |num| match num {
      Token::Number(num) => Ok(*num),
      Token::Operator(_) => Err(Error::Evaluate(String::from("Missing result"))),
    },
  )
}

fn calculate(expr: &str) -> Result<i64, Error> {
  evaluate(postfix(tokenize(&expr)?)?)
}

fn as_hex(mut number: i64) -> VecDeque<i64> {
  let mut hex = VecDeque::<i64>::new();
  while number > 0 {
    hex.push_front(number % 256);
    number /= 256;
  }
  hex
}

fn as_bin(mut number: i64) -> VecDeque<u8> {
  let mut bits = VecDeque::<u8>::new();
  while number > 0 {
    bits.push_front((number & 1) as u8);
    number >>= 1;
  }

  while bits.len() % 4 != 0 {
    bits.push_front(0)
  }

  bits
}

fn as_dec_str(number: i64) -> String {
  format!("dec {}", number)
}

fn as_oct_str(number: i64) -> String {
  format!("oct {:o}", number)
}

fn as_hex_str(number: i64) -> String {
  let mut hex_string = String::from("hex ");

  for byte in as_hex(number).iter() {
    hex_string.push_str(&format!("{:02X}", byte));
  }

  hex_string
}

fn as_bin_str(number: i64) -> String {
  let mut bin_string = String::from("bin ");

  let mut n = 0;
  let bits = as_bin(number);
  while n < bits.len() {
    for four_bits in as_bin(number).iter().skip(n).take(4) {
      bin_string.push_str(&format!("{}", four_bits));
    }
    bin_string.push(' ');
    n += 4;
  }
  bin_string.pop();

  bin_string
}

fn parse_args(args: &[String]) -> String {
  if !args.is_empty() {
    args
      .iter()
      .skip(1)
      .map(|s| s.chars().filter(|c| !c.is_whitespace()).collect::<String>())
      .join("")
  } else {
    String::new()
  }
}

fn main() {
  let expr: Vec<_> = env::args().collect();
  match calculate(&parse_args(&expr)) {
    Ok(res) => {
      println!("{}", as_dec_str(res));
      println!("{}", as_hex_str(res));
      println!("{}", as_oct_str(res));
      println!("{}", as_bin_str(res));
    }
    Err(err) => {
      println!("{:?}", err);
    }
  }
}

#[cfg(test)]
// clippy reporst const as never used
#[allow(dead_code)]
mod test {

  use super::*;

  const ZERO: Token = Token::Number(0);
  const ONE: Token = Token::Number(1);
  const TWO: Token = Token::Number(2);
  const THREE: Token = Token::Number(3);
  const FOUR: Token = Token::Number(4);
  const FIVE: Token = Token::Number(5);
  const SEVEN: Token = Token::Number(7);
  const EIGHT: Token = Token::Number(8);
  const TEN: Token = Token::Number(10);
  const ELEVEN: Token = Token::Number(11);
  const TWELVE: Token = Token::Number(12);
  const THIRTEEN: Token = Token::Number(13);
  const FOURTEEN: Token = Token::Number(14);
  const FIFTEEN: Token = Token::Number(15);

  const HUNDRED1: Token = Token::Number(100);
  const HUNDRED2: Token = Token::Number(200);

  const THOUSAND1: Token = Token::Number(1000);
  const THOUSAND10: Token = Token::Number(10000);

  const ADD: Token = Token::Operator('+');
  const SUB: Token = Token::Operator('-');
  const MULTI: Token = Token::Operator('*');
  const DIV: Token = Token::Operator('/');
  const OPEN: Token = Token::Operator('(');
  const CLOSE: Token = Token::Operator(')');

  #[test]
  fn args_to_string_ok() {
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from("1+1")].to_vec()),
      "1+1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from("1 + 1")].to_vec()),
      "1+1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from("1-1")].to_vec()),
      "1-1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from(" 1 - 1")].to_vec()),
      "1-1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from("1*1")].to_vec()),
      "1*1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from("1 *1")].to_vec()),
      "1*1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from("1/1")].to_vec()),
      "1/1"
    );
    assert_eq!(
      parse_args(&[String::from("devcalc"), String::from(" 1 / 1 ")].to_vec()),
      "1/1"
    );
  }

  #[test]
  fn tokenize_ok_cases() {
    assert_eq!(tokenize("1+1"), Ok([ONE, ADD, ONE].to_vec()));
    assert_eq!(tokenize("10*10"), Ok([TEN, MULTI, TEN].to_vec()));
    assert_eq!(tokenize("200-100"), Ok([HUNDRED2, SUB, HUNDRED1].to_vec()));
    assert_eq!(
      tokenize("10000/1000"),
      Ok([THOUSAND10, DIV, THOUSAND1].to_vec())
    );
    assert_eq!(
      tokenize("4*5+8/2-10"),
      Ok([FOUR, MULTI, FIVE, ADD, EIGHT, DIV, TWO, SUB, TEN].to_vec())
    );
    assert_eq!(tokenize("-1+-1"), Ok([SUB, ONE, ADD, SUB, ONE].to_vec()));
    assert_eq!(
      tokenize("(1+1)+2"),
      Ok([OPEN, ONE, ADD, ONE, CLOSE, ADD, TWO].to_vec())
    );
  }

  #[test]
  fn tokenize_hex_ok_cases() {
    assert_eq!(tokenize("0xA"), Ok([TEN].to_vec()));
    assert_eq!(tokenize("0XB"), Ok([ELEVEN].to_vec()));
    assert_eq!(
      tokenize("0x0EEEEEEEEEEEEEEE"),
      Ok([Token::Number(1076060070966390510)].to_vec())
    );
    assert_eq!(tokenize("0x1+0XC"), Ok([ONE, ADD, TWELVE].to_vec()));
    assert_eq!(tokenize("2-0x0D"), Ok([TWO, SUB, THIRTEEN].to_vec()));
    assert_eq!(tokenize("4*0X000E"), Ok([FOUR, MULTI, FOURTEEN].to_vec()));
    assert_eq!(tokenize("8/0X00000F"), Ok([EIGHT, DIV, FIFTEEN].to_vec()));
  }

  #[test]
  fn tokenize_octal_ok_cases() {
    assert_eq!(tokenize("07"), Ok([SEVEN].to_vec()));
    assert_eq!(tokenize("00"), Ok([ZERO].to_vec()));
    assert_eq!(
      tokenize("073567356735673567356"),
      Ok([Token::Number(1076060070966390510)].to_vec())
    );
    assert_eq!(tokenize("012+12"), Ok([TEN, ADD, TWELVE].to_vec()));
    assert_eq!(tokenize("015-0x0A"), Ok([THIRTEEN, SUB, TEN].to_vec()));
    assert_eq!(
      tokenize("00010*0X000E"),
      Ok([EIGHT, MULTI, FOURTEEN].to_vec())
    );
    assert_eq!(tokenize("012/02"), Ok([TEN, DIV, TWO].to_vec()));
    assert_eq!(tokenize("012+0"), Ok([TEN, ADD, ZERO].to_vec()));
  }

  #[test]
  fn tokenize_octal_nok_cases() {
    assert_eq!(
      tokenize("08"),
      Err(Error::Parse(String::from("Incorrect number")))
    );
    assert_eq!(
      tokenize("008"),
      Err(Error::Parse(String::from("Incorrect number")))
    );
  }

  #[test]
  fn tokenize_binary_ok_cases() {
    assert_eq!(tokenize("0b1"), Ok([ONE].to_vec()));
    assert_eq!(tokenize("0b1"), Ok([ONE].to_vec()));
    assert_eq!(
      tokenize("0b111011101110111011101110111011101110111011101110111011101110"),
      Ok([Token::Number(1076060070966390510)].to_vec())
    );
    assert_eq!(tokenize("0b1010+0b1100"), Ok([TEN, ADD, TWELVE].to_vec()));
    assert_eq!(tokenize("0b1101-0b1010"), Ok([THIRTEEN, SUB, TEN].to_vec()));
    assert_eq!(
      tokenize("0b1000*0b1110"),
      Ok([EIGHT, MULTI, FOURTEEN].to_vec())
    );
    assert_eq!(tokenize("0b1010/0b0010"), Ok([TEN, DIV, TWO].to_vec()));
    assert_eq!(tokenize("0b1010+0"), Ok([TEN, ADD, ZERO].to_vec()));
  }

  #[test]
  fn tokenize_binary_nok_cases() {
    assert_eq!(
      tokenize("0b1102"),
      Err(Error::Parse(String::from("Incorrect number")))
    );
    assert_eq!(
      tokenize("0b"),
      Err(Error::Parse(String::from("Incorrect number")))
    );
  }

  #[test]
  fn tokenize_nok_cases() {
    assert_eq!(
      tokenize("1+1a"),
      Err(Error::Parse(String::from("Incorrect number")))
    );
    assert_eq!(
      tokenize("1$1"),
      Err(Error::Parse(String::from("Not recognize input")))
    );
  }

  #[test]
  fn postfix_ok() {
    assert_eq!(
      postfix([ONE, ADD, ONE].to_vec()),
      Ok([ONE, ONE, ADD].to_vec())
    );
    assert_eq!(
      postfix([ONE, SUB, ONE].to_vec()),
      Ok([ONE, ONE, SUB].to_vec())
    );
    assert_eq!(
      postfix([ONE, ADD, ONE, ADD, ONE].to_vec()),
      Ok([ONE, ONE, ADD, ONE, ADD].to_vec())
    );
    assert_eq!(
      postfix([ONE, SUB, ONE, SUB, ONE].to_vec()),
      Ok([ONE, ONE, SUB, ONE, SUB].to_vec())
    );
    assert_eq!(
      postfix([TWO, MULTI, TWO, ADD, TWO, MULTI, TWO].to_vec()),
      Ok([TWO, TWO, MULTI, TWO, TWO, MULTI, ADD].to_vec())
    );
    assert_eq!(
      postfix([FOUR, DIV, TWO, SUB, TWO, MULTI, TWO].to_vec()),
      Ok([FOUR, TWO, DIV, TWO, TWO, MULTI, SUB].to_vec())
    );
    assert_eq!(
      // 1+(1*2)
      postfix([ONE, ADD, OPEN, ONE, MULTI, TWO, CLOSE].to_vec()),
      Ok([ONE, ONE, TWO, MULTI, ADD].to_vec())
    );
    assert_eq!(
      // 2*(3+2)
      postfix([TWO, MULTI, OPEN, THREE, ADD, TWO, CLOSE].to_vec()),
      Ok([TWO, THREE, TWO, ADD, MULTI].to_vec())
    );
    assert_eq!(
      // (1+2)
      postfix([OPEN, ONE, ADD, TWO, CLOSE].to_vec()),
      Ok([ONE, TWO, ADD].to_vec())
    );
    assert_eq!(
      // (2*(3+2))*(10+2)
      postfix(
        [OPEN, TWO, MULTI, OPEN, THREE, ADD, TWO, CLOSE, CLOSE, MULTI, OPEN, TEN, ADD, TWO, CLOSE]
          .to_vec()
      ),
      Ok([TWO, THREE, TWO, ADD, MULTI, TEN, TWO, ADD, MULTI].to_vec())
    );
    assert_eq!(
      // ((2*(3+2))+10)*2
      postfix(
        [OPEN, OPEN, TWO, MULTI, OPEN, THREE, ADD, TWO, CLOSE, CLOSE, ADD, TEN, CLOSE, MULTI, TWO]
          .to_vec()
      ),
      Ok([TWO, THREE, TWO, ADD, MULTI, TEN, ADD, TWO, MULTI].to_vec())
    );
  }

  #[test]
  fn evaluate_ok() {
    assert_eq!(evaluate([ONE, ONE, ADD].to_vec()), Ok(2));
    assert_eq!(evaluate([ONE, ONE, SUB].to_vec()), Ok(0));
    assert_eq!(evaluate([ONE, TWO, SUB].to_vec()), Ok(-1));
    assert_eq!(evaluate([TWO, TWO, MULTI].to_vec()), Ok(4));
    assert_eq!(evaluate([TWO, TWO, DIV].to_vec()), Ok(1));
  }

  #[test]
  fn evaluate_nok() {
    assert_eq!(
      evaluate([TWO, ZERO, DIV].to_vec()),
      Err(Error::Evaluate(String::from("Divide by zero")))
    );
  }

  #[test]
  fn as_hex_ok() {
    assert_eq!(as_hex(10), [10]);
    assert_eq!(as_hex(255), [255]);
    assert_eq!(as_hex(256), [1, 0]);
    assert_eq!(as_hex(65535), [255, 255]);
    assert_eq!(as_hex(65536), [1, 0, 0]);
  }

  #[test]
  fn as_bin_ok() {
    assert_eq!(as_bin(1), [0, 0, 0, 1]);
    assert_eq!(as_bin(2), [0, 0, 1, 0]);
    assert_eq!(as_bin(15), [1, 1, 1, 1]);
    assert_eq!(as_bin(256), [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]);
  }

  #[test]
  fn as_dec_str_ok() {
    assert_eq!(as_dec_str(10).as_str(), "dec 10");
    assert_eq!(as_dec_str(65535).as_str(), "dec 65535");
  }

  #[test]
  fn as_hex_str_ok() {
    assert_eq!(as_hex_str(10).as_str(), "hex 0A");
    assert_eq!(as_hex_str(255).as_str(), "hex FF");
    assert_eq!(as_hex_str(256).as_str(), "hex 0100");
    assert_eq!(as_hex_str(65535).as_str(), "hex FFFF");
    assert_eq!(as_hex_str(65536).as_str(), "hex 010000");
  }

  #[test]
  fn as_oct_str_ok() {
    assert_eq!(as_oct_str(10).as_str(), "oct 12");
    assert_eq!(as_oct_str(65536).as_str(), "oct 200000");
  }

  #[test]
  fn as_bin_str_ok() {
    assert_eq!(as_bin_str(10).as_str(), "bin 1010");
    assert_eq!(as_bin_str(256).as_str(), "bin 0001 0000 0000");
  }
}
